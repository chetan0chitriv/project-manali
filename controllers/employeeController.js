const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const { update } = require('../models/employeeModel');
const Employee = mongoose.model('Manali');

router.get('/', (req, res) => {
    res.render("employee/addOrEdit", {
        viewTitle : "Insert Employee"
    });
});

router.post('/',(req,res) => {
    if (req.body._id == '')
    insertRecord(req.body);
    else
    updateRecord(req, res);
});

// router.post('/',(req,rest) => {
//     const employee  = new Employee({
//       _id:new mongoose.Types.ObjectId,
//       name:req.body.name,
//       email:req.body.email,
//       mobile_No:req.body.mobile_No,
//       address:req.body.address,
//       study:req.body.study,
         
//     })


function insertRecord(req,res) {
    const employee = new Employee();
    employee.fullName = req.body.fullName,
    employee.email = req.body.email,
    employee.mobile = req.body.mobile,
    employee.city = req.body.city
    employee.save((err, doc) => {
        if (!err)
            res.redirect('employee/list');
        else {
            if(err.name == validationError ){
            handleValidationError(err, req.body);
            res.render("employee/addOrEdit", {
                viewTitle : "Insert Employee",
                employee: req.body
            });
         }
            else
            console.log('Error during record inseration :' + err);
        }
    });
    }



function updateRecord(req, res){
    Employee.findOneAndUpdate({ _id: req.body._id}, req.body, { new:true }, (err, doc) => {
        if (!err) 
        {res.redirect('employee/list'); }
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("employee/addOrEdit", {
                    viewTitle: 'Update Employee',
                    employee: req.body
                });
            }
            else
            console.log('Error during record update : ' + err);
        }

    });
}



router.get('/list', (req, res) => {
    Employee.find((err, docs) => {
        if (!err) {
            res.render('employee/list',{
                list: docs
            });
        }
        else {
            console.log('Error in retrieving employee list :' + err);
        }

    });
});

function handleValidationError(err,body){
    for(field in err.errors)
    {
      switch (err.errors[field].path){
          case 'fullName':
              body['fullNameError'] = err.errors[field].massage;
              break;
              case 'email':
                  body['emailError'] = err.errors[field].massage;
                  break;
      }  
    }
}

router.get('/:id', (req,res) =>{
    Employee.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.render("employee/addOrEdit", {
                viewTitle: "Update Employee",
                employee: doc
            });

        }
    });

});
module.exports = router;