const mongoose = require('mongoose');
const employeeSchema = new mongoose.Schema({

   fullName:{
       type: String
   },
   email:{
       type: String
   },
   mobile:{
       type: Number
   },
   city:{
       type: String
   }
});


employeeSchema.path('email').validate((val) => {
    return emailRegex.test(val);
}, 'Invalid e-mail.');

module.exports = mongoose.model('Manali', employeeSchema);
